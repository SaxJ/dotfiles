#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '
# >>> Added by cnchi installer
BROWSER=/usr/bin/firefox
EDITOR=/usr/bin/nvim

### MINE
#PS1='\e[37;1m\u@\H \W \$\e[0m '

# VIM
if [ -x "$(command -v nvim)" ]; then
    alias vim="nvim"
    alias vi="nvim"
fi

# GO
export GOPATH=/home/saxonj/go_packages

# RUST
export CARGO_PATH=/home/saxonj/.cargo/bin

# PYTHON
#export WORKON_HOME=$HOME/.virtualenvs
#export PROJECT_HOME=$HOME/Devel
#source /usr/bin/virtualenvwrapper.sh

# HELPER FUNCTIONS

# PATH
PATH="$PATH:/home/saxonj/bin:$GOPATH/bin:$CARGO_PATH:/home/saxonj/.local/bin:/home/saxonj/.npm-global/bin"

# HOW DO?
alias hd="howdoi"

# EXCERSISM
if [ -f ~/.config/exercism/exercism_completion.bash ]; then
    source ~/.config/exercism/exercism_completion.bash
fi

alias yt-audio="youtube-dl -x --audio-format vorbis --audio-quality 0"
alias aws='sudo docker run --rm -t $(tty &>/dev/null && echo "-i") -e "AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}" -e "AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}" -e "AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION}" -v "$(pwd):/project" mesosphere/aws-cli'

# GIT GOOD
alias ga='git add -i'
alias gc='git ci'
alias resolve='grep -lr "<<<<<<<" . | xargs git checkout'

# KUBE MADNESS
function resolve-pod() {
 echo $(kubectl get pods -o name | sed 's/^pod\///g' | grep "^$1" | head -1)
}

function kube() {
    if [[ "$#" < 2 ]]; then
        kube-fwd "$1"
    else
        POD=$(resolve-pod $2)
        if [ -z "$POD" ]; then
            POD=$(resolve-pod "heweb-$2")
        fi

        if [ -z "$POD" ]; then
            echo "No such pod: $2"
            return 1
        else
            kube-fwd "$1" "$POD" "${@:3}"
        fi
    fi
}

function kube-fwd() {
    COMMAND=$1
    shift

    DIRECT_FUNCTION="kube-$COMMAND"

    if type "$DIRECT_FUNCTION" | grep -q 'is a shell function'; then
        echo "$DIRECT_FUNCTION" "$@"
        $DIRECT_FUNCTION "$@"
    else
        echo kubectl "$COMMAND" "$@"
        kubectl "$COMMAND" "$@"
    fi
}

function kube-shell() {
    ${2?"You gotta specify a container too."}
    kubectl exec $1 -it -c $2 -- "bash"
}

kube-up() { minikube start; }
kube-down() { minikube stop; }
kube-status() { minikube status && kubectl get pods; }
kube-tail() { kubectl logs $1 --all-containers -f; }
kube-exec() { kubectl exec -it $1 "${@:2}"; }
kube-sql() { kube exec heweb-postgres psql $*; }
kube-artisan() { kube-exec $1 -c php-fpm /var/www/quadra/healthengine.com.au/lib/he-artisan "${@:2}"; }

# GIT PROMPT
GIT_PROMPT_ONLY_IN_REPO=1
source ~/.bash-git-prompt/gitprompt.sh
